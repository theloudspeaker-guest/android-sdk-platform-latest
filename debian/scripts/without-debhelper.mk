# For debugging

# Tips: Run these targets in a new shell as AOSP's `build/envsetup.sh` will
# override the default `make`

export ANDROID_APILEVEL = 28

.PHONY: build clean orig-tar

init:
	quilt push -a
	$(RM) --recursive .pc
	debian/scripts/prepare-prebuilts
	debian/scripts/remove-unwanted-bpmk

build:
	debian/scripts/build # Can be used for incremental builds

clean:
	$(RM) --recursive out prebuilts .pc external/doclava
	- repo forall -c git checkout -- "."
	- git checkout -- external frameworks build

orig-tar:
	tar --create --xz --file ../upstream.orig.tar.xz --exclude-from=debian/orig.excludes --mtime=2000-01-01 --sort=name *